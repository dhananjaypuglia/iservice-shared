// import {createAction} from 'redux-actions';

// ******************
//  ACTION CONSTANTS
// ******************

// ******************************************
//  Naming convention for actions calling api
// ACTION_NAME_INITIATED: Dispatched before api call
// ACTION_NAME_SUCCESS: Dispatched on api success
// ACTION_NAME_FAILED: Dispatched on api failure
// ******************************************