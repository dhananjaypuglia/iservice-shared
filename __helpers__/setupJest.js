// this file will be called once before starting the test suite
import Adapter from 'enzyme-adapter-react-16';
import {configure} from 'enzyme';

jest.unmock('react-redux'); // This is done to make the existing tests pass, removing this line will make all the test cases fail where the page component is being imported
configure({adapter: new Adapter()});
