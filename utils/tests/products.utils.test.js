import {getBalances} from '../products.utils';

describe('Calculate Products check sum and isChecked status accordingly', () => {

  it('getBalances: Should calculate the get balances properly', () => {
    const payload = getBalances('1200');
    const result = {balance: 1200, isChecked: true, checkedBillSum: 1200};
    expect(payload).toEqual(result);
  });
  
  it('getBalances: Should set isChecked to false when balance is negative', () => {
    const payload = getBalances('-1200');
    const result = {balance: -1200, isChecked: false, checkedBillSum: -1200};
    expect(payload).toEqual(result);
  });
  
  it('getBalances: Should calculate balances accordingly when invalid balance is provides', () => {
    const payload = getBalances('null');
    const result = {balance: 0, isChecked: false, checkedBillSum: 0};
    expect(payload).toEqual(result);
  });
});
  