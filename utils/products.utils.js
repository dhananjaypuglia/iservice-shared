export const getBalances = (balance) => ({
  balance: parseFloat(balance) || 0.0,
  isChecked: parseFloat(balance) > 0,
  checkedBillSum: parseFloat(balance) || 0.0
});